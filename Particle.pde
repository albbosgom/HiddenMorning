 
class Particle {
  PVector loc;
  PVector vel;
  PVector acc;
  int age;
   
  Particle(float lx, float ly, float vx, float vy, float ax, float ay) {
    loc = new PVector(lx, ly);
    vel = new PVector(vx, vy);
    acc = new PVector(ax, ay);
    age = 0;
  }
  void draw() {
    fill(
      random(128,255), random(0, 170), random(0,50), random(0,20)
    );
    ellipse(loc.x, loc.y, 5, 20);
 
    acc.set(p.loc.x-loc.x, 0, 0);
    acc.mult(particleAccDamp);
    vel.add(acc);
    vel.add(flameGravity);
    loc.add(vel);
    age++;
  }
   
}
 
Particle freshParticle() {
  return new Particle(p.loc.x, p.loc.y+p.r/2-20,
  //return new Particle(100, 100,
                      random(-3, 3), random(-3, 0),
                      0, 0);
}
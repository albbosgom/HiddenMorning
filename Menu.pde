// Pretty self-explanatory, we just draw the menu here.
void menu(){
  update();
  background(menuBackground);
  
  image(menuOptions, width-50, height-50);
  pushMatrix();
  if(rectOver){
    fill(highlightColor);
  }
  else{
    fill(baseColor);
  }
  
  
  stroke(19,108,137);
  blendMode(EXCLUSION);
  rectMode(CENTER);  
  rect(rectX, rectY, rectSize*3, rectSize);
  popMatrix();
  
  blendMode(NORMAL);
  textFont(narrator);
  textAlign(CENTER);
  fill(5,52,66);
  text("New Game", rectX, rectY+10);
}

// This method checks if the cursor is on the start button constantly.
void update(){
  if(overRect(rectX, rectY, rectSize*3, rectSize)){
    rectOver=true;
  }else{
    rectOver=false;
  }
}

/* A method to check if the mouse is inside a rectangle. The parameters are the
x and y coordinates of the upper left corner, the width and the height of the
rectangle.
*/
boolean overRect(int x, int y, int wid, int hei){
  if (mouseX >= x-wid/2 && mouseX <= x+wid/2 && 
      mouseY >= y-hei/2 && mouseY <= y+hei/2) {
    return true;
  } else {
    return false;
  }
}

void options(){  
     background(optionsBackground);
     textFont(narrator);
     textAlign(LEFT);     
     if(lang == Language.SPANISH)
     fill(255);
     else fill(5,52,66);
     text("Spanish", width/3+60,height/5);
     if(lang == Language.ENGLISH)
     fill(255);
     else fill(5,52,66);
     text("English", width/3+250,height/5);
     fill(5,52,66);    
     textAlign(RIGHT);
     text("Language:", width/3,height/5);
     text("Volume:", width/3,height*2/5);
     text("-", width/3+20,height*2/5);
     text("+", width/3+lineLength+70,height*2/5+5);
     text("Return", width-20,height-50);
     stroke(0);
     line(width/3+30, height*2/5-10, width*2/3, height*2/5-10);
     lineLength = width*2/3-width/3-30;
     line(width/3+30, height*2/5, width/3+30, height*2/5-20);
     line(width/3+30+lineLength/5, height*2/5, width/3+30+lineLength/5, height*2/5-20);
     line(width/3+30+lineLength*2/5, height*2/5, width/3+30+lineLength*2/5, height*2/5-20);
     line(width/3+30+lineLength*3/5, height*2/5, width/3+30+lineLength*3/5, height*2/5-20);
     line(width/3+30+lineLength*4/5, height*2/5, width/3+30+lineLength*4/5, height*2/5-20);
     line(width/3+30+lineLength, height*2/5, width/3+30+lineLength, height*2/5-20);
     stroke(255);
     if(volume == 0.0){
       line(width/3+30+lineLength, height*2/5, width/3+30+lineLength, height*2/5-20);
     }
     else if(volume == 1.0/5){
       line(width/3+30+lineLength*4/5, height*2/5, width/3+30+lineLength*4/5, height*2/5-20);       
     }
     else if(volume == 2.0/5){
       line(width/3+30+lineLength*3/5, height*2/5, width/3+30+lineLength*3/5, height*2/5-20);      
     }
     else if(volume == 3.0/5){
       line(width/3+30+lineLength*2/5, height*2/5, width/3+30+lineLength*2/5, height*2/5-20);       
     }
     else if(volume == 4.0/5){
       line(width/3+30+lineLength/5, height*2/5, width/3+30+lineLength/5, height*2/5-20);      
     }
     else{
       line(width/3+30, height*2/5, width/3+30, height*2/5-20);      
     }
  
}
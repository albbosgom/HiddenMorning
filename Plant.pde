
// LIGHT BUD

void lightBud(float h, float xoff, int branch, float growth, float posX, float posY) {
  blooming = true;
  if(branch>=0){
  // thickness of the branch is mapped to its length
  float sw = map(h, 2, 100, 1, 5);
  strokeWeight(sw);
  // Draw the branch
  if(growth<1) {
    line(0, 0, 0, -h*growth);
    currentGrowth+=0.02;
  }
  else if(growth>0){
    line(0, 0, 0, -h);
    if(branch==0&&growth>=1){
       numberOfBranch++; 
    }
  }
  // Move along to end
  translate(0, -h);

  // Each branch will be 2/3rds the size of the previous one
  posY+=h;
  h *= 0.7;
  
  // Move along through noise space
  xoff += 0.1;

  if (h > 4) {
    // Random number of branches
    //int n = int(random(0, 5));
    int n = 1;
    for (int i = 0; i < n; i++) {
      
      // Here the angle is controlled by perlin noise
      // This is a totally arbitrary way to do it, try others!
      float theta = map(noise(xoff+i, yoff), 0, 1, -PI/3, PI/3);
      if (n%2==0) theta *= -1;
      
      pushMatrix();      // Save the current state of transformation (i.e. where are we now)
      rotate(theta);     // Rotate by theta
      lightBud(h, xoff, branch-1, growth-1, posX, posY);   // Ok, now call myself to branch again
      popMatrix();       // Whenever we get back here, we "pop" in order to restore the previous matrix state
    }
  }
  else{
    pushMatrix();
    fill(255);
    ellipse(0,0,radius,radius);    
    translate(0, -h);
    if(radius<20) radius+=0.05;
    else if(flashlightEnabled) flashlightEnabled = false;
    popMatrix();
    
    
    if (radius > 0) {
      fill(255, alphaBud);
      ellipse(0,0,width*2,width*2);
      if(alphaBud<255 && radius <20){
      alphaBud+=1;
      }
      
    }  //<>//
    if(radius >=20){
      alphaBud-=1;
      
    }
    if(alphaBud <=-200)
    if(!bloomed)    
      showDialogue();
    bloomed = true;
      
  }
    
  }
    

    Ani.to(this, 2, "x", width/2);
    Ani.to(this, 2, "y", height-posY); 
  
}

// SAKURA TREE

class SakuraTree {
  int generations;
  int n;
  int slots;
  int nextSlot;
  Branch[] branches;
  int[] order; // order of branches to be drawn to draw in strait lines
  int[] dummie; // recording accounted nodes for sorting
  int sCntr; // counter for sorting branches
  int branchDrawn;
  Position[] trail; // array of points being drawn and fading out
  int trailLength; // length of fade trail
  int fadeCounter; // holds position in array that is being itterated through

  SakuraTree() {
    generations = 7;
    n = 1;
    slots = floor(pow(2, generations+1))-1;
    nextSlot = 1;
    branches = new Branch[slots];
    order = new int[slots];
    dummie = new int[slots];
    sCntr = 1;
    branchDrawn = 0;
    trailLength = 30;
    trail = new Position[trailLength];
    fadeCounter = 0;
  }
  void init() {    
    branches[0] = new Branch(0, round(random(1, 3)), new Position(mouseX, height+0.0), PI/2);
    branches[0].setOrigin( new Position(mouseX, height+0.0) );
    pushMatrix();
    stroke(#332010);
    fill(0);
    for(int i=0; i<slots; i++) {
      while(branches[i] != null && branches[i].steps > 0) {
        branches[i].createStep();
      }
      if(nextSlot <= slots - 2) {
        branches[nextSlot] = branches[i].generateChild(0);
        nextSlot += 1;
        branches[nextSlot] = branches[i].generateChild(1);
        nextSlot += 1;
      }  //<>//
      branches[i].active = false;
    }
    popMatrix();
    noStroke();
    for(int i=0; i<slots; i++) {
      this.dummie[i] = 1;
    }
    for(int i=0; i<trailLength; i++) {
      trail[i] = new Position(0.0, (float)-generations);
    }
    this.setOrder();
  }
  void setOrder() {
    for (int i=0; i<slots; i++) {
      if(sCntr>=slots) {
        sCntr = 1;
        while(dummie[sCntr-1] == 0) {
          sCntr++;
        }
      }
      int rand = (int)random(0,2);
      order[i] = sCntr-1;
      dummie[sCntr-1] = 0;
      sCntr = sCntr*2;
      sCntr += rand;
    }
  }
  Position drawTree() {
    
    if(branchDrawn>=255 && stairCount >=450)
    activateEnding();
    if(branchDrawn < slots) {
      Position nextPoint = branches[order[branchDrawn]].drawStep();
      if(nextPoint == null) {
        branchDrawn++;
        if( branches[order[branchDrawn-1]].getGeneration() > generations - 2) {
          return trail[fadeCounter];
        }
      }
      else {
        nextPoint.d += nextPoint.d*noise(frameCount)*0.5;
        trail[fadeCounter] = nextPoint;
        fade();
      }
      if(order[branchDrawn]*2 > slots) {
        
      }
    }
    return null;
  }
  void fade() {
    for(int i=0; i<trailLength; i++) {
      stroke(0,90);
      for(int j=0; j<trailLength; j+=10) {
        int k = (fadeCounter+trailLength - j)%trailLength;
        strokeWeight(this.trail[k].d*j/trailLength);
      }
    }
    strokeWeight(this.trail[fadeCounter].d);
    point(this.trail[fadeCounter].x , this.trail[fadeCounter].y);
    fadeCounter++;
    if(fadeCounter >= trailLength) {
      fadeCounter = 0;
    }
  }
}

public class Position {
  public float x;
  public float y; 
  public float d; // diameter of position
  Position(float ax, float ay) {
    this.x = ax;
    this.y = ay;
  }
  void setDiameter(float d) {
    this.d = d;
  }
}

public class Branch {
  public int generation;
  public int steps;
  private int maxSteps;
  private float stepLength;
  public Position position;
  public Position drawFrom;
  public Position drawTo;
  public Position point2draw;
  public float angle;
  public float maxAngleVar = 0.2;
  public boolean active = true;
  public float lnth;
  public int cntr;
  /*****************************************************************************************
  Branch
  Parameters:
  gen: generation of the tree that this branch lives on
  ******************************************************************************************/
  Branch(int gen, int mstep, Position p, float ang) {
    this.generation = gen;
    this.maxSteps = mstep;
    this.steps = mstep;
    this.stepLength = 100.0/(this.generation+1);
    this.position = p;
    this.drawFrom = new Position(0.0, 0.0);
    this.drawTo = new Position(0.0, 0.0);
    this.point2draw = new Position(0.0, 0.0);
    this.angle = ang;
    this.lnth = 0;
    this.cntr = 0;
  }
  public void createStep() {
    float r = random(-1, 1);
    this.angle = this.angle + this.maxAngleVar*r;
    this.stepLength = this.stepLength - this.stepLength*0.2;
    this.position.x += this.stepLength*cos(this.angle);
    this.position.y -= this.stepLength*sin(this.angle);
    this.drawTo.x = this.position.x;
    this.drawTo.y = this.position.y;
    this.point2draw.x = this.drawFrom.x;
    this.point2draw.y = this.drawFrom.y;
    this.lnth = sqrt(pow( this.drawTo.x-this.drawFrom.x, 2 ) + pow( this.drawTo.y-this.drawFrom.y, 2 ));
    strokeWeight(floor(20/(this.generation+1)));
    this.steps = this.steps - 1;
  }
  public Position drawStep() {
    stroke(0); 
    float diameter = floor(20/(this.generation+1)) - (cntr)/lnth*(floor(20/(this.generation+1)) - floor(20/(this.generation+2)));
    strokeWeight(diameter);
    this.point2draw.x = cntr/lnth*(this.drawTo.x - this.drawFrom.x) + drawFrom.x;
    this.point2draw.y = cntr/lnth*(this.drawTo.y - this.drawFrom.y) + drawFrom.y;
    if( cntr < lnth) {
      cntr++;
      point2draw.setDiameter(diameter);
      return point2draw;
    }
    return null;
  }
  public Branch generateChild(int cn) {
    int newGen = this.generation + 1;
    float angleShift = 0.5;
    if (cn == 1) {
      angleShift = angleShift*(-1);
    }
    float childAngle = this.angle+angleShift;
    float px = this.position.x;
    float py = this.position.y;
    Position parentPos = new Position(px, py);
    Branch child = new Branch(newGen, floor(random(1, 4)), parentPos, childAngle);
    child.setOrigin( this.drawTo );
    return child;
  }
  public void setOrigin( Position p ) {
    this.drawFrom = p;
  }
  /*****************************************************************************************
  getGeneration
  Returns the generation that this branch is in
  ******************************************************************************************/
  public int getGeneration() {
    return generation;
  }
}

class Blossoms {
  ArrayList blossoms; // container for all the blossoms that are to be made
  Blossom currentBlossom; // current blossom that is being drawn
  int currentBlossomPlace; // location of current  blossom in the blossoms array
  int bpb; // number of blossoms that are generated per branch
  Blossoms() {
    blossoms = new ArrayList();
    currentBlossom = null;
    bpb = 15;
  }
  void addBlossom( Position p ) {
    // creat a random collection of blossoms to grow on that branch
    for(int i=0; i<bpb; i++) {
      // create blossom
      Position nP = new Position(p.x+random(0,30)-15, p.y+random(0,30)-15);
      Blossom b = new Blossom(nP,10);
      // add new blossom to arrayList
      blossoms.add(b);
    }
    if(currentBlossom == null) {
      currentBlossomPlace = 0;
      currentBlossom = (Blossom) blossoms.get(currentBlossomPlace);
    }
  }
  void drawBlossoms() {
    // draw currently growing blossom
    if(currentBlossom != null) {
      currentBlossom.drawBlossom();
      if(currentBlossom.finished()) {
        blossoms.remove(currentBlossomPlace);
        if(blossoms.size() <= 0) {
          currentBlossom = null;
          currentBlossomPlace = 0;
        }
        else {
          currentBlossomPlace = (int)random(0,blossoms.size());
          currentBlossom = (Blossom) blossoms.get(currentBlossomPlace);
        }
      }
    }
  }
}

class Blossom {
  Position p; // position of blossm
  float d; // diameter of blossom
  float t; // timer: lifespan of the blossom before it falls from the tree
  color c; // color of the blossom to be drawn
  Blossom(Position p, float d) {
    this.p = p;
    this.d = d;
    this.t = d;
    // randomly set the color of the blossom
    c = setColor();
  }
  void drawBlossom() {
    if(t>0) {
      pushMatrix();
      noStroke();
      fill(this.c);
      ellipseMode(CENTER);
      ellipse(p.x, p.y, d/t, d/t);
      popMatrix();
      t--;
    }
  }
  boolean finished() {
    if(t > 0) {
      return false;
    }
    return true;
  }
  color setColor() {
    int isColor = (int)random(0,3);
    if( isColor == 0 ) {
      return color(242,175,193,100);
    }
    else if( isColor == 1 ) {
      return color(237,122,158,100);
    }
    return color(229,76,124,100);
  }
}
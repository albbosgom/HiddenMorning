// This is the class that is used to generate the lamp.

class Lamp  {

  PVector loc;      // Location of Lamp ball
  PVector loc1;      // Location of point on arc
  PVector origin;   // Location of arm origin
  float r;           // Length of arm


  float ballr;       // Ball radius
  float damping;     // Arbitary damping amount
  float maxedisp;
  float theta1;
  float lineLength;
  float angle;   
  
  // Constructor here.
  
  Lamp(PVector origin_, float r_) {
    // Fill all variables
    origin = origin_.get();
    r = r_;
    theta = 0.0;
    mass = 1.0f;
    
    //calculate the location of the ball using polar to cartesian conversion
    float x = r * sin(theta);
    float y = r * cos(theta);
    loc = new PVector(origin.x + x, origin.y + y);
    loc1= new PVector(r*sin(theta), r*cos(theta));
    theta_vel = 0.0f;
    theta_acc = 0.0f;
    damping = 1.0f;   // Arbitrary damping
    ballr = 100.0f;    // Arbitrary ball radius
    yshiftpe=150;
    yshiftke=150;
    mpe = 0.0f;
    mke = 0.0f;
    tote = 0.0f;
    maxedisp = 2.0 * r;
    maxpe = mass * scaling*G * maxedisp;
    theta1=0.0;
    
   theta = 0.05;                      // Angle relative to vertical axis
   loc.set(r*sin(theta),r*cos(theta),0);         // Polar to cartesian conversion
   loc.add(origin);                              // Make surea the location is relative to the Lamp's origin
   tote =  mass * scaling*G * (loc.y - r - origin.y) * -1.0;
    
  }

  void go() {
    update();
    render();
    
  }

  // Function to update location
  void update() {
    theta_acc = (-1 * G*scaling / r) * sin(theta);      // Calculate acceleration (see: http://www.myphysicslab.com/Lamp1.html)
    theta_vel += theta_acc;                     // Increment velocity
    theta_vel *= damping;                       // Arbitrary damping
    theta += theta_vel;                        // Increment theta

    
    loc.set(r*sin(theta),r*cos(theta),0);         // Polar to cartesian conversion
    loc.add(origin);                              // Make sure the location is relative to the Lamp's origin
   
    mpe = (mass * scaling*G * (loc.y - r - origin.y) * -1.0);
    mke = tote - mpe;
    maxpe = mass * scaling*G * maxedisp;
    
  }
  /* Render function. It draws the line from the ceiling, and the lamp, which
  keeps rotating slightly as if it was swinging.*/
  
  void render() {
    sketchbook();
    stroke(0);
    strokeWeight (2);
    // Draw the arm
    line(origin.x,origin.y,loc.x,loc.y);
    pushMatrix();
    
    rectMode(CENTER);
    // Draw the ball
    lineLength = sqrt((sq(origin.x-loc.x)+sq(origin.y-loc.y)));
    angle = (origin.x-loc.x)/lineLength;
    translate(loc.x,loc.y);
    rotate(asin(angle));
    fill(0);
    rect(0,0,ballr,ballr*2);
    popMatrix();
  }
}
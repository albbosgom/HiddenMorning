// This is the method used for controlling the lamp.

void lamp(){
  // We use the library Ani for a smooth animation of the lamp.
  p.go();
  Ani.to(this, 0.5, "x", p.loc.x);
  Ani.to(this, 0.5, "y", p.loc.y);
  flashlight();
  
  // And here we add the fire to the lamp, which are particles.  
  fill(255);
  noStroke();
  if (particles.size() < maxParticles) {
    for (int i = 0; i < addParticlesPerFrame; i++) {
      particles.add(freshParticle());
    }
  }
  // And if they die, we just simply create them again. 
  Particle p;
  for (int i = 0; i < particles.size(); i++) {
    p = (Particle)particles.get(i);
    p.draw();
    if (p.age > maxParticleAge) {
      particles.set(i, freshParticle());
    }
  }
}
  
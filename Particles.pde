class Trail{ //<>//
  
  float origX, origY, posX, posY;
  int life;
  
  Trail(float positionX, float positionY){
    origX = positionX;
    origY = positionY;
    posX = mouseX;
    posY = mouseY;
    life = 5;
    
  }
  
  void render(){
    pushMatrix();
    stroke(255*noise(mouseX),255*noise(mouseY), 255*noise(mouseX, mouseY), 255*life/5);
    strokeWeight(3);
    blendMode(NORMAL);
    line(origX, origY,posX,posY); //<>//
    life--;
    popMatrix();
  }
  
  
}
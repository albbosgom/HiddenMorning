/* IMPORTS*/ //<>//

import java.io.*;
import de.looksgood.ani.*;
import ddf.minim.*;


// Menu & misc
boolean rectOver, fade, fadein, right;
State state;
boolean menu = true;
int rectX, rectY, time;
int  rectSize = 90;
int alpha, alphaBud, nextLine = 0;
color baseColor, highlightColor;
PImage menuBackground, menuOptions, optionsBackground;
float colorIncrement = 1;
PFont narrator;
String filename;
StringList dialogue;
BufferedReader bufferedReader;
float angleLamp, jitter;
PImage flashlight, strongFlashlight, background;
boolean flashlightEnabled = true;
float volume = 0;
float lineLength;
Language lang = Language.ENGLISH;
// Lamp

Lamp p;  
float arm = 225.0f; //arm length


float mass = 1.0;
float G = 9.81;      // Arbitrary universal gravitational constant
float theta;       // Pendulum arm angle
float theta_vel;   // Angle velocity
float theta_acc;   // Angle acceleration
int yshiftpe, yshiftke;
float mpe, mke, tote, maxpe, maxke;  //variables associated with energy
int pause = 0;
float scaling = 0.05;
//float scaling2 = 0.5;
boolean strongLight = false;

// Flame


PVector flameGravity = new PVector(0, -1);
float particleAccDamp = 0.04;
int maxParticles = 1000;
int maxParticleAge = 15;
int addParticlesPerFrame = 30;
ArrayList particles;

// Light bulb

PShape bulb; 
PImage cir;
boolean destruct = false;
ArrayList<BrokenBulb> brokenParts;
int lightIntensity = 255;
float speed = 0;
float gravity = 0.2;

// stem

PShape stem;
float x, y, currentX, currentY;
float yoff = 0;
int seed = 5;
ArrayList<Trail> trails;

boolean halo = false;
float haloX, haloY;
float haloRadius;
float haloOpacity;


boolean bloomed, blooming = false;
float currentGrowth = 0.0;
int numberOfBranch = 0;
float radius = 0.0;
boolean grow = false;
float plantEnd;

PShape forest;

// Stairs

int num, cnt, px, py;
Stairs[] stairs;
boolean initialised=false;
float lastRelease=-1;
int stairCount = 0;
// Sakura Tree


ArrayList<SakuraTree> trees;
ArrayList<Blossoms> blossoms;
boolean tree = false;

// Universe

boolean started;
int ringCount;
float j;
float s, iStep, amp, maxAmp;


Power power;


// Sounds
Minim minim;
AudioPlayer click;
AudioPlayer shatter;
AudioPlayer electricity;
AudioPlayer menuMusic;
AudioPlayer music;
boolean fadeMusic = false;
float gain = 1.0;



void setup() {
  // Aplication settings 
  surface.setTitle("Hidden Morning");
  size(1280, 720, FX2D);
  //fullScreen(FX2D);

  // initialization of variables
  state = State.MENU;
  menuBackground = loadImage("menu_background.png");
  menuBackground.resize(width, height);
  menuOptions = loadImage("options.png");
  menuOptions.resize(50,50);
  optionsBackground = loadImage("options_background.png");
  optionsBackground.resize(width, height);
  stem = createShape(GROUP);

  // Lamp settings  

  p = new Lamp(new PVector(width/2, 0), arm); // Make a new Pendulum with an origin location and armlength
  particles = new ArrayList();

  // Light bulb settings

  bulb = loadShape("light bulb.svg");
  PGraphics pg = createGraphics(200, 200);
  pg.beginDraw();
  pg.fill(255);
  pg.noStroke();
  pg.fill(255);
  pg.blendMode(OVERLAY);

  pg.ellipse(100, 100, 160, 160);

  pg.filter(BLUR, 12);
  pg.filter(INVERT);
  pg.endDraw();
  cir = pg.get();
  imageMode(CENTER);


  // Fade settings
  fade = false;
  fadein = false;

  // Menu settings
  baseColor = color(69,58,54);
  highlightColor = color(51);
  rectX = width/2-10;
  rectY = height-rectSize*2;

  // Narrator settings.
  narrator = createFont("Arial", 42, true);
  filename = "narrator.txt";
  bufferedReader = createReader(filename);
  dialogue = new StringList();

  // Effects attached to the mouse. Resizes depending on the resolution.
  flashlight = loadImage("flashlight.png");
  double resizeWidthFactor= (double)width/1920;
  double resizeHeightFactor = (double)height/1080;
  flashlight.resize((int)(flashlight.width*resizeWidthFactor), (int)(flashlight.height*resizeHeightFactor));
  strongFlashlight = flashlight;
  strongFlashlight.resize(strongFlashlight.width*2, strongFlashlight.height*2);

  Ani.init(this);
  x = width/2;
  y = 100;


  brokenParts = new ArrayList<BrokenBulb>();

  // Game background settings
  background = loadImage("background.jpg");
  background.resize(width, height);

  background(255);
  currentX=mouseX;
  currentY=mouseY;
  trails = new ArrayList<Trail>();

  // Forest
  forest = createShape(GROUP);

  // Stairs  

  cnt=0;
  num=150;
  stairs=new Stairs[num];
  for (int i=0; i<num; i++) stairs[i]=new Stairs();
  px=-1;
  py=-1;

  // Sakura Tree  

  trees = new ArrayList<SakuraTree>();
  blossoms = new ArrayList<Blossoms>();

  // universe

  started = false;
  ringCount = 6;
  j = 0;
  iStep = 2;
  amp = 0;
  maxAmp = 500;

  // sounds
  minim = new Minim(this);
  click = minim.loadFile("bgm/click.mp3");
  shatter = minim.loadFile("bgm/shatter.mp3");
  electricity = minim.loadFile("bgm/power_down.mp3");
  menuMusic = minim.loadFile("bgm/menu_music.mp3");
  music = minim.loadFile("bgm/music.mp3");
  menuMusic.loop();
}


void draw() {
  
  fadeOut(false, 5.0, 6000);
  if (state == State.MENU) {
    menu();
  }

  if (state==State.NARRATOR && millis()-time > 6000) {
    narrator();
    if (menuMusic.getGain()==-80) {
      menuMusic.pause();
      menuMusic.setGain(0);
      music.loop();
    }
  }
  if(state == State.OPTIONS){
    options();
  }

  if (state == State.LAMP) {    
    clear();
    lamp();
    narrator();
    fadeOut(false, 5.0, 1000);
  }
  if (state == State.BUD) {
    sketchbook();
    if (flashlightEnabled) flashlight();    
    if (grow) growBud();
	// Now some conditions to avoid text when the bud is blooming.
    if (!blooming)
      narrator();
    else if (bloomed && nextLine>23)
      narrator();
  }
  
  if (state == State.LIGHTBULB) {
    strongLight = true;
    x = width/2+30;
    sketchbook(); // Sketchbook effect
    flashlight(); // Strong flashlight for the bulb!
	lightbulb(); // We load the light bulb now.
    destruct(); // And the method for destructing it later.
    narrator();
  } 

  if (state == State.GAME) {
    halo = false; // We disable the halo effect since we are not constantly rendering now.
    if (nextLine > 30) {
    if (keyPressed) {
      if (key=='1')
        power = Power.STAIRS;
      if (key == '2' && nextLine >=31)
        power = Power.SAKURA_TREE;
    }    
      growTrees();
      createStairs();
    }
    narrator();
  }

  if (state == State.ENDING) {
    universe();
    if (lightIntensity>0) {
      lightIntensity-=4;
    }
    fill(255, lightIntensity); // First we have a flash, which reduces very rapidly.
    rectMode(CORNER);
    rect(-1, -1, width+1, height+1);
    narrator();
  }

  mouseParticles();
  mouseHalo();
}

void mouseClicked() {
  haloX = mouseX;
  haloY = mouseY;
  haloRadius = 0.0;
  haloOpacity = 255;
  halo = true;
  if (state == State.MENU) {
    if (rectOver) {
      fade = true;
      time = millis();
      menuMusic.shiftGain(0, -80.0, 6000);
    }
    if(overRect(width-50,height-50,50,50)){
      state = State.OPTIONS;
    }
  }  
  if(state == State.OPTIONS){    
    if(overRect(width-120,height-50,120,50)){
      state = State.MENU;
    }
    if(overRect(width/3+20,height*2/5, 50, 50)){
       if(volume<1) //<>//
       volume=volume+0.2;
    }
    
    if(overRect(int(width/3+lineLength+70),height*2/5+5, 50, 50)){      
       if(volume>0)
       volume=volume-0.2;
    }
    
    
    if(overRect(width/3+60,height/5, 170, 50)){      
       lang = Language.SPANISH;
    }
    
    if(overRect(width/3+250,height/5, 170, 50)){      
       lang = Language.ENGLISH;
    }
    
    
   click.setGain(volume*-80); //<>//
   shatter.setGain(volume*-80);
   electricity.setGain(volume*-80);
   menuMusic.setGain(volume*-80);
   music.setGain(volume*-80);
  }
  if (state == State.NARRATOR && millis()-time > 6000) {
    showDialogue();
  }

  if (state == State.LAMP && millis()-time > 5000) {
    showDialogue();
  }

  if (state == State.BUD) {

    if (nextLine ==23) {
      seeding(Seed.LightBud);
      if (bloomed)
        showDialogue();
    } else
      showDialogue();
  }

  if (state == State.LIGHTBULB) {
    if (millis()-time > 5000) {
      showDialogue();
    }
  }

  if (state == State.GAME) {
    if(nextLine >=31){
      if (power == Power.STAIRS)
        initialised = true;
      if (power == Power.SAKURA_TREE) {
  
        SakuraTree t = new SakuraTree();
        t.init();
        trees.add(t);
        blossoms.add(new Blossoms());
        tree = true;
      }
    }

    if (nextLine != 28 && nextLine !=31)
      showDialogue();
  }
  if (state == State.ENDING)
    showDialogue();
}   

void mouseMoved() {
  if (state != State.GAME) { // The only state where we don't want the trail effect is the Game state, as it doesn't constantly render the background.
    trails.add(new Trail(currentX, currentY));
    currentX=mouseX;
    currentY=mouseY;
    if (state==State.BUD) {
      if (grow == false) {
        Ani.to(this, 1.5, "x", mouseX);
        Ani.to(this, 1.5, "y", mouseY);
      }
    }
  }
}

void mouseReleased() {
  if (nextLine == 28) 
    showDialogue();
}

void exit() {
	/* Whenever we exit, we stop the music plugin. Otherwise the window closes
	but the program does not. We don't want unclosable music, don't we?*/
   minim.stop(); 
   super.exit();
}

// The fade out effect. Used for smooth transitions between states.
void fadeOut(boolean fadeInOut, float fadeSpeed, int milliseconds) {

  //If we want the fade method to work:
  if (fade == true) {    
    menu = false; // we disable the menu
    alpha+=fadeSpeed; // And increase our alpha according to the speed we want.
    fill(0, alpha);
    rect(0, 0, width*2, height*2);
    if (state==State.MENU) state=State.NARRATOR;
    int this_instant = millis();
    // Now if we have waited enough time...
    if (this_instant-time > milliseconds) {  
      fade=false; // We disable the fade effect
      if (state == State.LAMP) {
        state = State.LIGHTBULB; //And if we were in the state lamp, we switch on the lights!
        click.play();
      }
      if (fadeInOut) fadein=true; // If we want an additional fade to white, we activate it now.
    }
  }

  if (fadein) {       
    fill(255, alpha);
    rect(0, 0, width*2, height*2);
    alpha+=fadeSpeed;
    if (alpha == 100) {
      fadein=false;
    }
  }
}

/* The sketchbook effect. Just takes a paper image as a background, and draws a bunch
 of supposedly randomized lines to give that animation sketch effect*/
void sketchbook() {
  pushMatrix();
  background(background);
  blendMode(OVERLAY);
  strokeWeight(1);
  stroke(0);
  for (int i=0; i<100; i++) {
    if (Math.random()<0.5) {
      line(random(0.0, width), random(0.0, height), random(0.0, width), random(0.0, height));
    } else {
      noFill();
      arc(random(0.0, width), random(0.0, height), random(0.0, width), random(0.0, height), radians(random(0, 360)), radians(random(0, 360)));
    }
  }
  popMatrix();
}

// The flashlight effect. It's just a black image with a big, transparent whole on the middle.
void flashlight() {

  imageMode(CENTER);
  blendMode(NORMAL);    
  if (!strongLight)
    image(flashlight, x, y);
  else image(strongFlashlight, x, y);
}

// This was meant to be a method for several types of seed, but in the end it only works with the lightbud. Oh well.
void seeding(Seed seed) {
  switch(seed) {
  case LightBud:
    if (grow==false) strongLight = true;
    grow = true;
  }
}

/* The broken bulb class! It represents a part of the broken bulb. It has a bunch of different
 methods for making it easier to control. They just go towards the ground in a random but consistent direction*/
class BrokenBulb {

  float posX;
  float posY;
  float angle;
  float partLength;
  boolean right;
  boolean line;

  BrokenBulb(float xCoordinate, float yCoordinate, float rotationAngle) {    
    posX = xCoordinate;
    posY = yCoordinate;
    angle = rotationAngle;    
    if (random(1)>0.5) right = true; // We decide randomly if the part ends flying out to the right or to the left.
    else right = false;
    if (random(1)<0.2) line = true; // and here we decide if it's a line or an arc. Randomly as well.
    else line = false;
    partLength = random(10, 35);
  }

  // Is our part flying out to the right? Used to update the position.
  boolean isRight() {
    return right;
  }

  // Is our part a line? Used for drawing it.
  boolean isLine() {
    return line;
  }
  float getX() {
    return posX;
  }
  float getY() {
    return posY;
  }
  void setX(float xCoord) {
    posX=xCoord;
  }

  void setY(float yCoord) {
    posY=yCoord;
  }

  float getAngle() {
    return angle;
  }
  float getLength() {
    return partLength;
  }
}

// Our destruct method! Used for the destruction of the light bulb with the previous class.
void destruct() {
  if (destruct) {
    clear();
    background(0);
    // Since it's inside the draw method, which is a while, we just have to check the condition each frame.
    if (lightIntensity>0) {
      lightIntensity-=4;
    }
    fill(255, lightIntensity); // First we have a flash, which reduces very rapidly.
    rectMode(CORNER);
    rect(-1, -1, width+1, height+1);
    int opacity = 255-int(255*speed/13);

    // And now for every part of the bulb, we draw it depending if it's a line or an arc.
    for (BrokenBulb partIn : brokenParts) {
      stroke(255, opacity);
      if (partIn.isLine()) {
        line(partIn.getX(), partIn.getY(), partIn.getX()+partIn.getLength()*cos(partIn.getAngle()), partIn.getY()+partIn.getLength()*sin(partIn.getAngle()));
      } else {
        noFill();
        float startingAngle = (partIn.getX()/partIn.getY())*partIn.getAngle();
        arc(partIn.getX(), partIn.getY(), partIn.getLength(), partIn.getLength(), startingAngle, startingAngle+partIn.getAngle());
      }
      // And now we update their position. Easy.
      partIn.setY(partIn.getY()+speed);
      if (partIn.isRight()) {          
        partIn.setX(partIn.getX()+speed*0.1);
      } else {
        partIn.setX(partIn.getX()-speed*0.3);
      }
    }
    speed+=gravity;
  }
}

// Simple method. Used for the particles of the after-images created by the mouse.
void mouseParticles() {
  for (Trail trail : trails) {    
    trail.render();
  }
}

/* This is the method for creating that halo that happens when you click somewhere.
 It certainly has room for improvement. Maybe trying a less aggressive change of colors.
 Also there could be the possibility of more than one halo on the screen at the same time.*/
void mouseHalo(){
  if(halo){
    if(haloRadius<60){
      noFill();
      ellipse(haloX,haloY,haloRadius,haloRadius);
      haloRadius+=2;
    }
    else if(haloOpacity>0){
      noFill();
      stroke(255, haloOpacity);
      ellipse(haloX,haloY,haloRadius,haloRadius);
      haloOpacity-=25;
    } 
  } 
}

 // Method for drawing the trees in the game state once they're created.
void growTrees(){
  if(tree){  
    for(int i=0;i<trees.size();i++){
      SakuraTree t = trees.get(i);
      Blossoms b = blossoms.get(i);
      Position p = t.drawTree();
      p = t.drawTree();
      // if a branch ended and a position was passed, set up a new set of blossoms to grow
      if (p!=null) {      
        b.addBlossom(p);
        b.drawBlossoms();      
      }
      b.drawBlossoms();
      b.drawBlossoms();
      loop();
    }
  }
}

/* The universe method. It makes a couple of circles, and makes particles
using a sin function. It also changes colors using HSB mode.*/
void universe(){
  if(j >= 360) j = 0;
    j+=0.2; 
    pushMatrix();
    background(#333333);
    noStroke();
    fill(#333333,10);
    rect(0,0,width, height);
    translate(width/2, height/2);
    
    amp+=0.01;
    rotate(radians(amp));
    for(int ring=0; ring<ringCount; ring++) {      
      float i = 0;
      while(i<360) {
        if(i>0) {
          pushMatrix();
          rotate(radians(i));
          noStroke();
          colorMode(HSB, 360, 100, 100);
          fill(j,100*ring/6, 100*ring/6);  
          if(ring%2 == 0) {
            s = sin(amp+ring+i+HALF_PI)*(10/(ring+1))+(20/(ring+1));
            x = sin(amp+ring+i)*maxAmp/pow(2,ring+1)+maxAmp*(2/pow(2,ring));
          } else {
            s = cos(amp+ring+i+HALF_PI)*(10/(ring+1))+(20/(ring+1));
            x = cos(amp+ring+i)*maxAmp/pow(2,ring+1)+maxAmp*(2/pow(2,ring));
          }          
          ellipse(x, 0, s, s);
          popMatrix();
        }
        i += iStep;
      }
    }
    popMatrix();
    colorMode(RGB);
}

//A method to set some things for the ending.  
void activateEnding(){
  if(!started){    
    started = true;
    lightIntensity=255;
    state = State.ENDING;
  }
}

// We initialize some stuff in order to start the growing bud.

void growBud(){
	pushMatrix();
	// Plant
	blendMode(NORMAL);
	stroke(0);
	// Start the tree from the bottom of the screen
	translate(width/2, height);
	// Move alogn through noise
	yoff += 0.005;
	randomSeed(seed);
	stem = createShape(GROUP);
	lightBud(100, 0, numberOfBranch, currentGrowth, 0, 0);
	popMatrix();
}

// The light bulb. It's in fact just a vectorial image.
void lightbulb(){
	
    image(cir, width/2, 50);
    pushMatrix();
    translate(width/2, 50);
    shapeMode(CENTER);
    fill(255, random(20, 150));
    blendMode(NORMAL);
    shape(bulb, 0, 0, -100, -125);
    popMatrix();
}
Hidden Morning Project.

A degree's final project using Processing. A short interactive story about a beautiful experience where you start in the darkness, but will grow out of that cold environment by growing your own garden.
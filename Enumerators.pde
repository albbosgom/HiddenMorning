// The finite-state machine of the game.
public enum State {
  MENU, OPTIONS, NARRATOR, GAME, LAMP, LIGHTBULB, BUD, ENDING, CREDITS  
}
// The two powers the user can use.
public enum Power {
  STAIRS, SAKURA_TREE  
}

/* This was originally thought to have more than one type of seed.
Now it's just there to make the code clearer I guess*/

public enum Seed{
   LightBud
}

public enum Language{
  ENGLISH, SPANISH
}
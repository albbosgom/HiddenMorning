/* The narrator method. It's used to display the next line of the narrator.
We load the file only once at the beginning of the program, and we read
using a pointer to the next line called nextLine*/ 

void narrator(){
    if(state == State.NARRATOR){
      // We read the file for the first time
      if(dialogue.size() == 0){
        try{      
          String line;         
          if(lang == Language.ENGLISH)
            filename = "narrator.txt";
          else if(lang == Language.SPANISH)
            filename = "narrator_es.txt";
          bufferedReader = createReader(filename);
          while((line = bufferedReader.readLine()) != null){
          dialogue.append(line);}
          bufferedReader.close();
          
        }
        catch(FileNotFoundException ex) {
           System.out.println("Unable to open file '"+ filename+"'"); 
        }
        catch(IOException ex){
           System.out.println("Error reading file '"+filename+"': ");
           ex.printStackTrace();
        }    
      }
      /* If we're in the narrator state, we want our background to be completely black
      constantly in order to avoid problems with the afterimage drawn by the cursor.*/
      background(0);
      fill(140,140,140, alpha);
      textAlign(CENTER);
      textFont(narrator);
      text(autoWrap(dialogue.get(nextLine)), width/2, height/4);
      if (alpha < 400) {
        alpha+=5;
      
      }
    }
    else{
      
      // Just checking the time for some smooth transitions.
      
      if(millis()-time>5000){
        
        /* And if we're not in the narrator state, we just draw the text. Depending on the
        next line, we'll want our text to be in the upper or in the lower part*/
        if(nextLine < dialogue.size()){
          fill(140,140,140, alpha);
          textAlign(CENTER);
          textFont(narrator);
          if(nextLine>=24)
          text(autoWrap(dialogue.get(nextLine)), width/2, height/4);
          else
          text(autoWrap(dialogue.get(nextLine)), width/2, height*3/4);
          if (alpha < 400) {
            alpha+=5;
          }
        }
        
      }
      
    }
}

// A simple method to avoid lines too long to be out of the window.

String autoWrap(String text){
  
  if(text.length()>45){
    
    String[] words = text.split("\\s+");
    String res = "";
    int n = 1;
    for(String word:words){
      
      if(res.length() + word.length() > 45*n){
        res = res+"\n"+word;
        n++;
      }
      else{
        res=res+" "+word;
      }
      
    }
    return res;
    
  }
  return text;  
}

/*This method is used when clicking and it's purpose is for changing the next line.
It's also used to change between states of the program*/

void showDialogue(){
    alpha = 0;
    nextLine++;
    if(nextLine == 8){
      state = State.LAMP;
      time = millis();
    }
    else if(nextLine == 14){  
      // Here we reset the time in order to do the fade for changing to the lamp.
      time = millis();  
      fade=true;
    }
    else if(nextLine == 17){     
      // We destroy the light bulb now! We also generate the broken parts. About 15 is good enough.
      destruct = true;
      time = millis();
      
      for(int i=0;i<15;i++){
         BrokenBulb temp = new BrokenBulb(width/2+random(-100,100), 200*random(1), random(0,TWO_PI));
         brokenParts.add(temp);
      }
      electricity.play();
      shatter.play(); 
    }
    else if(nextLine == 20){
      state = State.BUD;      
    }
    else if(nextLine >= 28 && nextLine<32){
      state = State.GAME;  
      clear();
      background(255);
      fade = false;
    }
    
    else if(nextLine >= 32){
      state = State.ENDING;      
    }
}
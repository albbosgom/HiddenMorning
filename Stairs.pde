
class Stairs {
  Vec2D v,vD;
  float dir,dirMod,speed;
  int col,age,stateCnt;
   
  Stairs() {
    v=new Vec2D(0,0);
    vD=new Vec2D(0,0);
    age=0;
  }
 
  void init(float _dir) {
    dir=_dir;
    float prob=random(100);
    noStroke();
    if(prob<80) age=15+int(random(30));
    else if(prob<99) age=45+int(random(50));
    else age=100+int(random(100));
     
    if(random(100)<80) speed=random(2)+0.5;
    else speed=random(2)+2;
 
    if(random(100)<80) dirMod=20;
    else dirMod=60;
     
    v.set(mouseX,mouseY);
    initMove();
    dir=_dir;
    stateCnt=10;
    if(random(100)>50) col=0;
    else col=1;
  }
     
  void initMove() {
    if(random(100)>50) dirMod=-dirMod;
    dir+=dirMod;
     
    vD.set(speed,0);
    vD.rotate(radians(dir+90));
 
    stateCnt=10+int(random(5));
    if(random(100)>90) stateCnt+=30;
  }
   
  void update() {
    age--;
    if(age>=30) {
      vD.rotate(radians(1));
      vD.mult(1.01f);
    }
    noStroke(); 
    v.add(vD);
    if(col==0) fill(255-age,0,100,150);
    else fill(100,200-(age/2),255-age,150);
    pushMatrix();
    translate(v.x,v.y);
    rotate(radians(dir));
    rect(0,0,1,16);
    popMatrix();
     
    if(age==0) {
      if(random(100)>50) fill(200,0,0,200);
      else fill(00,200,255,200);
      float size=2+random(4);
      if(random(100)>95) size+=5;
      ellipse(v.x,v.y,size,size);
      stairCount++;
    }
    if(v.x<0 || v.x>width || v.y<0 || v.y>height) age=0;
     
    if(age<30) {
      stateCnt--;
      if(stateCnt==0) {
        initMove();
      }
    }
   }
   
}

void createStairs(){
  
  
  if(mousePressed) {
    int i;
    float dir;
     
    if(px==-1) {
      px=mouseX;
      py=mouseY;
      dir=0;
    }
    else if(px==mouseX && py==mouseY) dir=int(random(36))*10;
    else dir=degrees(atan2(mouseY-py,mouseX-px))-90;
   
    i=0;
    while(i<num) {
      if(stairs[i].age<1) {
        stairs[i].init(dir);
        break;
      }
      i++;
    }
     
    px=mouseX;
    py=mouseY;
  }
   
  for(int i=0; i<num; i++)
    if(stairs[i].age>0) stairs[i].update();
  
}